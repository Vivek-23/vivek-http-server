const http = require('http');
const uuid = require('uuid');
const fs = require('fs');


const server = http.createServer((request, response) => {
    let url = request.url.split('/');

    switch (url[1]) {
        case '': {
            response.write('Server Created Successfully');
            response.end();
            break;
        }


        case 'html': {

            fs.readFile("./index.html", function (error, pageData) {
                if (error) {
                    response.writeHead(500, {
                        'Content-Type': 'text/html'
                    });
                    response.write('Error : Server Not Found');
                } else {
                    response.writeHead(200, {
                        'Content-Type': 'text/html'
                    });
                    response.write(pageData);
                }

                response.end();
            });

            break;
        }

        case 'json': {

            fs.readFile("./sample.json", function (error, jsonData) {
                if (error) {
                    response.writeHead(500, {
                        'Content-Type': 'application/json'
                    });
                    response.write('Error : Server Not Found');
                } else {
                    response.writeHead(200, {
                        'Content-Type': 'application/json'
                    });
                    response.write(jsonData);
                }

                response.end();
            });

            break;
        }

        case 'uuid': {
            response.setHeader('Content-Type', 'application/json');
            response.write(JSON.stringify(
                {
                    'uuid': uuid.v4()
                }
            ));

            response.end();
            break;
        }

        case 'status': {
            response.setHeader('Content-Type', 'application/json');
            let bool = false;
            for (let code in http.STATUS_CODES) {
                /**
                 *  url[2] contain the status code number which it used to compare with original 
                 *  status code present in http.
                 */
                if (code == url[2]) {
                    response.write(JSON.stringify({ "STATUS": http.STATUS_CODES[code] }));
                    bool = true;
                    break;
                }
            }

            if (!bool) {

                response.write(JSON.stringify({
                    "STATUS": 'Invalid status code'
                }));
            }

            response.end();
            break;
        }

        case 'delay': {
            response.setHeader('Content-Type', 'application/json');
            const regex = /^[0-9]+$/;
            /**
             *  url[2] contain the time which user have declare in url and multiply with 1000
             *  to convert into miliseconds.
             */
            if (url[2].match(regex) != null) {
                let time = parseInt(url[2]) * 1000;
                response.setTimeout(time, () => {
                    response.end(JSON.stringify({ '200': http.STATUS_CODES['200'] }));
                });
            } else {
                response.writeHead(500, {
                    'Content-Type': 'application/json'
                });
                response.write(JSON.stringify({
                    '500': 'Server not Found'
                }));
                response.end();
            }

            break;
        }

        default: {

            response.writeHead(404, {
                'Content-Type': 'text/html'
            });
            response.write('<h1>404 : Page Not Found</h1>');
            response.end();
        }
    }
});

server.listen(3000);